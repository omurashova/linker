package aichallenge.simulator;

import java.io.FileInputStream;
import java.io.IOException;

public class Map {
	public int size = 25;
	public String[][] matrix;

	public Map() throws IOException {

		this.matrix = new String[this.size][this.size];
		FileInputStream inFile = new FileInputStream("map/map.txt");
		byte[] str = new byte[inFile.available()];
		inFile.read(str);
		String text = new String(str);
		String[] numbers = text.split(" ");

		inFile.close();
		for (int i = 0; i < this.size; i++)
			for (int j = 0; j < this.size; j++)
				this.matrix[i][j] = this.next(numbers);

	}

	public void printM() {
		for (int i = 0; i < this.size; i++)
			for (int j = 0; j < this.size; j++)
				System.out.print(this.matrix[i][j] + " ");
		System.out.println();

	}

	private int currentIndex = -1;

	private String next(String numbers[]) {
		++currentIndex;
		while (currentIndex < numbers.length
				&& numbers[currentIndex].equals(""))
			++currentIndex;

		return currentIndex < numbers.length ? numbers[currentIndex] : null;
	}

	public void RandomFirstAnt() { // ����� ����� ������������� �����

	}
}
