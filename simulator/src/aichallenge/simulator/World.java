package aichallenge.simulator;

import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

class AffableThread implements Runnable {
	@Override
	public void run() // ���� ����� ����� �������� � �������� ������
	{
		System.out.println("������ �� ��������� ������!");
	}
}

public class World {

	public static Map mapOriginal;
	public static Map mapWork;
	private int idAnt = 0;
	private JSONObject obj;
	private FileWriter file;

	public int CountOfPlayer = 0;

	public World(String game_id) {
		try {
			this.mapOriginal = new Map();
			this.mapWork = new Map();
			obj = new JSONObject();
			// file = new FileWriter("log/test.json");
			file = new FileWriter("log/" + game_id + ".json");
			// ��������� ������ ������������, � � ��� �������� ��������
		} catch (IOException e) {
			System.out.print("Exception ");
		}

	}

	private AffableThread[] listThread;

	public void WordSimulation() {
		// listThread = new AffableThread[CountOfPlayer];
		// Players pl = new Players();
		for (int count = 0; count < 2; count++) {
			for (int i = 1; i < 2; i++) {
				SwitchPlayer(i);
			}
			PushMatrixJsonFileLog(this.mapWork.matrix);
		}
		try {
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean GameEnd() {
		return true;
	}

	public void RandomAnt() {

		for (int pl = 1; pl <= CountOfPlayer; pl++) {
			String find = "H" + pl;
			for (int i = 0; i < mapWork.size; i++) {
				for (int j = 0; j < mapWork.size; j++) {
					if (find.equals(mapWork.matrix[i][j])) {
						if (j + 6 < mapWork.size - 1) {
							this.mapWork.matrix[i + 3][j + 3] = "A" + pl
									+ GenIdAnt();
							SwitchPlayerAddAnt(pl,
									this.mapWork.matrix[i + 3][j + 3], i + 3,
									j + 3);

						}

						if (j + 6 > mapWork.size - 1) {
							this.mapWork.matrix[i - 3][j - 3] = "A" + pl
									+ GenIdAnt();
							SwitchPlayerAddAnt(pl,
									this.mapWork.matrix[i - 3][j - 3], i - 3,
									j - 3);
						}
					}
				}
			}
		}

	}

	public void SwitchPlayerAddAnt(int num, String id, int x, int y) {
		switch (num) {
		case 1: {
			Main.firstPlayer.listAnt.add(new Ant(id, x, y));
		}
		case 2: {
			Main.secondPlayer.listAnt2.add(new Ant(id, x, y));
		}
		case 3: {
		}
		case 4: {
		}
		case 5: {
		}
		case 6: {
		}
		case 7: {
		}

		}
	}

	public void SwitchPlayer(int num) {
		switch (num) {
		case 1: {
			Main.firstPlayer.Function();
		}
		case 2: {
			Main.secondPlayer.Function();
		}
		case 3: {
		}
		case 4: {
		}
		case 5: {
		}
		case 6: {
		}
		case 7: {
		}

		}
	}

	private String GenIdAnt() {
		return Integer.toString(this.idAnt++);
	}

	public void PushMatrixJsonFileLog(String[][] map) {

		JSONArray list = new JSONArray();

		for (int i = 0; i < map.length; i++) {
			for (int j = 0; j < map.length; j++) {
				list.add(map[i][j] + " ");
			}
		}
		obj.put("map", list);
		try {

			file.write(obj.toString());
			file.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
