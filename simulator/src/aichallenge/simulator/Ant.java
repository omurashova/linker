package aichallenge.simulator;

public class Ant {
	public Ant(String id, int x, int y) {
		setId(id);
		this.CoordX = x;
		this.CoordY = y;
	}

	private String Id;
	private int IdAnthill;

	public String getId() {
		return Id;
	}

	public int getIdAnthill() {
		return IdAnthill;
	}

	public void setId(String n) {
		this.Id = n;
	}

	public void setIdAnthill(int n) {
		this.IdAnthill = n;
	}

	private int �ealth = 3;

	public int getHealth() {
		return �ealth;
	}

	public void set�ealth(int n) {
		this.�ealth = n;
	}

	private int CoordX;
	private int CoordY;

	public int getCoordX() {
		return CoordX;
	}

	public int getCoordY() {
		return CoordY;
	}

	public void StepLeft() {
		World.mapWork.matrix[CoordX][CoordY] = World.mapOriginal.matrix[CoordX][CoordY];
		this.CoordY++;
		World.mapWork.matrix[CoordX][CoordY] = this.Id;
	}

	public void StepRight() {
		World.mapWork.matrix[CoordX][CoordY] = World.mapOriginal.matrix[CoordX][CoordY];
		this.CoordY--;
		World.mapWork.matrix[CoordX][CoordY] = this.Id;
	}

	public void StepUp() {
		World.mapWork.matrix[CoordX][CoordY] = World.mapOriginal.matrix[CoordX][CoordY];
		this.CoordX--;
		World.mapWork.matrix[CoordX][CoordY] = this.Id;
	}

	public void StepDown() {
		World.mapWork.matrix[CoordX][CoordY] = World.mapOriginal.matrix[CoordX][CoordY];
		this.CoordX++;
		World.mapWork.matrix[CoordX][CoordY] = this.Id;
	}

	public void Battle() {
	}

	public void TakeFood(Anthill home) {
	}

	public String[][] SeeMap;

	public String[][] getSeeMap() {

		// System.out.println(CoordX+" "+CoordY);
		this.SeeMap = new String[5][5];
		for (int i = 0; i < 5; i++)
			for (int j = 0; j < 5; j++) {
				SeeMap[i][j] = "0";
			}
		// World world = new World();
		for (int i = this.CoordX - 2, in = 0; i < this.CoordX + 2; i++, in++)
			for (int j = this.CoordY - 2, jn = 0; j < this.CoordY; j++, jn++) {
				SeeMap[in][jn] = World.mapWork.matrix[i][j];
			}

		return SeeMap;

	}

}
