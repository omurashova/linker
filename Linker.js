var knox = require('knox'),
    fs = require('fs'),
    http = require('http'),
    url = require('url'),
    qs = require('querystring'),
    exec = require('child_process').exec;

/**
* @author Olga Murashova <olga.mur2910@gmail.com>
* @class Linker
* @classdesc The Linker instance is declarated only once
* @this Linker
*/
var Linker = {
    /**
     * A client for interraption with AWS S3
     * @memberof Linker
     * @type {object}
     * @property {string} key A key for bucket on AWS S3
     * @property {string} secret A secret for bucket on AWS S3
     * @property {string} bucket The bucket's name
     */
    s3_client : knox.createClient({
        key: 'AKIAJS7ZQO2ZLLKYGRQQ',
        secret: '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',
        bucket: 'accountfiles'
    }),
    /**
     * An array to name users's files in a right way for the simulator
     * @memberof Linker
     * @type {object[]}
     */
    players : {
        '0': 'FirstPlayer',
        '1': 'SecondPlayer',
        '2': 'ThirdPlayer',
        '3': 'FourthPlayer',
        '4': 'FifthPlayer',
        '5': 'SixthPlayer',
        '6': 'SeventhPlayer'
    },
   /**
   * The main method starting the server
   * @memberof Linker
   * @method run
   */
    run : function () {
      this.server = http.createServer(function (req, res) {
          var qurl, users;
          if (req.method != 'GET') {
              return;
          }
          qurl = url.parse(req.url);
          if(qurl.pathname != '/simulate')
              return;
          console.log('get query ' + qurl.query);
          query = qs.parse(qurl.query);
          if(typeof query.players == 'string') {
              res.write('simulation has failed');
              console.log('simulation has failed');
              res.end();
              return;
          }
          var flag = 0;
          Linker.getUserFiles(query.players, function(status) {
              if(status == 'failed') {
                  flag ++;
                  if(flag > 1)
                     return;
                  res.write('simulation has failed');
                  console.log('simulation has failed');
                  res.end();
                  return;
              }
              Linker.simulate(query.game_id, function (status) {
                  var message = status == 'success' ? 'simulation has finished' : 'simulation has failed';
                  res.write(message);
                  console.log(message);
                  res.end();
              });
          });
      }).listen(3001);
      console.log('listening on ' + this.server.address().address + '/' + this.server.address().port);
    },

 /**
 * @callback Linker~simulationCallback
 * @param  {string} status - Contains information about simulation's result 'success' or 'failed'
 */
  /**
   * This method starts simulator
   * @memberof Linker
   * @method simulate
   * @param {string} game_id An identificator of the current game
   * @param {Linker~simulationCallback} callback - The callback that handles the result of the simulation
   */
    simulate : function (game_id, callback) {
        console.log('simulating game ' + game_id);
        child = exec('ant -f ./simulator/build.xml Main -Dgame=' + game_id,
            function (error, stdout, stderr) {
                console.log('stdout: ' + stdout);
                console.log('stderr: ' + stderr);
                if (error !== null) {
                  console.log('exec error: ' + error);
                  return;
                }
                fs.readFile('./simulator/log/' + game_id + '.json', function(err, buf){
                    var req = Linker.s3_client.put('/' + game_id + '.json', {
                        'Content-Length': buf.length
                      , 'Content-Type': 'text/plain'
                    });
                    req.on('response', function(res){
                       if (200 == res.statusCode) {
                          console.log('saved to %s', req.url);
                          callback('success');
                       }
                       else
                          callback('failed');
                    });
                    req.end(buf);
                });
            });
        //Test.test2(game_id, callback);
    },

  /**
   * @callback Linker~getUserFilesCallback
   * @param  {string} status - Contains information about getting users's files 'success' or 'failed'
   */
  /**
   * This method gets users's files from AWS S3 in a loop
   * @memberof Linker
   * @method getUserFiles
   * @param {object} players An array of users for one game
   * @param {Linker~getUserFilesCallback} callback - The callback that handles the result of getting users's files
   */
    getUserFiles : function (players, callback) {
        var count = 0;
        var t = this;
        for (var i in players) {
            this.downloadUserFile(i, players[i], function (done, user) {
                if (!done) {
                    console.log('undone ' + user);
                    callback('failed');
                }
                if (done) {
                  console.log('done ' + user + ' ' + count);
                  count ++;
                  if(count == players.length) {
                      console.log('all files are loaded!!!!');
                      callback('success');
                  }
                }
            });
        }
    },

      /**
   * @callback Linker~downloadUserFileCallback
   * @param  {string} status - Contains information about downloading current file 'done' or 'undone'
   */
  /**
   * This method downloads one user's file from Amazon S3 and puts into the specified folder for the simulator
   * @memberof Linker
   * @method downloadUserFile
   * @param {int} i An id of a current user in a loop
   * @param {string} user An e-mail of a current user
   * @param {Linker~downloadUserFileCallback} callback - The callback that handles the result of downloading current user's file
   */

    downloadUserFile : function (i, user, callback) {
        this.s3_client.get('/test2/' + user + '/' + 'code.java').on('response', function (res) {
            if (res.statusCode != '200') {
                callback(false, user);
                return;
            }
            res.setEncoding('utf8');
            res.on('data', function (chunk) {
                // fs.writeFile('var/uploads/' + Linker.players[i] + '.java', chunk, function(err) {
                  fs.writeFile('simulator/src/aichallenge/simulator/' + Linker.players[i] + '.java', chunk, function(err) {
                    if(!err)
                        callback(true, user);
                     else
                        callback(false, user);
                });
            });
        }).end();
    }
}

Linker.run();

// var Test = {
//     test1: function () {
//         console.log('executing test1');
//         var options = {
//             port: Linker.server.address().port,
//             method: 'get',
//             path: '/simulate?players=olga.mur2910@gmail.com&players=batiscshev.pavel@gmail.com&game_id=001'
//         };
//         var req = http.request(options, function(res) {
//             res.on('data', function (data) {
//                 console.log(data.toString());
//             });
//         });
//         req.end();
//     },
//     test2: function (game_id, callback) {
//         console.log('executing test2');
//         child = exec('node test.js ' + game_id,
//             function (error, stdout, stderr) {
//                 console.log('stdout: ' + stdout);
//                 console.log('stderr: ' + stderr);
//                 if (error !== null) {
//                   console.log('exec error: ' + error);
//                   return;
//                 }
//                 fs.readFile('var/logs/log', function(err, buf){
//                     var req = Linker.s3_client.put('/' + game_id + '.log', {
//                         'Content-Length': buf.length
//                       , 'Content-Type': 'text/plain'
//                     });
//                     req.on('response', function(res){
//                        if (200 == res.statusCode) {
//                           console.log('saved to %s', req.url);
//                           callback('success');
//                        }
//                        else
//                           callback('failed');
//                     });
//                     req.end(buf);
//                 });
//             });
//     }
// }

// Test.test1();